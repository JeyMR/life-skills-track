# Full Text Search


## Abstract

This paper studies the full-text search, which is a query-searching technique, as opposed to traditional search methods. It compares two leading open-source search engines that implement the full-text search technique on top of lucene which is a Java library that provides core functionalities for the full-text search.


## Introduction

Full-text search can be used to query unstructured data very efficiently which is not possible by the traditional querying method which only searches the exact matches. There are several methods such as

* Natural language processing
* Synonym expansion
* Ontologies and taxonomies
* Fuzzy matching
* Relevance ranking

that are applied by full-text search which all make it more capable of fetching the relevant data from very deep exploration of unstructured data. The core of full-text search is a full-text search index, it is a specialized data structure that enhances the ability to fetch data very efficiently from large volumes of textual data. This data structure contains the mapping between the analyzed text and their respective documents, so the text from the queried phrase will be matched against the index to quickly retrieve the appropriate documents. The same analyzer has been used for both the text in documents and text in queries.


## Elasticsearch

Elastic search is an open-source search and analytic engine built on top of lucene. It can store, search, and analyze vast amounts of data very efficiently. It is possible because it stores all the information as documents which are grouped by indices and it uses a special data structure called inverted indices, similar to hashmap, which links words to their respective documents. It is used for website search, business analytics, logging, monitoring, etc. as it is better suited for time series data analysis.


## Solr

Solr is an open-source enterprise search engine built on top of Lucene. It provides efficient caching, faceting, and sorting which make it suitable for e-commerce-like applications as it efficiently handles static data. It provides functionalities such as Indexing, Querying, Mapping, and Ranking.


## Conclusion

Choosing a particular search engine completely depends on the type of organization. If an organization is similar to e-commerce which deals with static data then Solr is a good choice as it provides efficient caching which facilitates quick retrieval of data and also provides faceting and sorting so that a user can search a specific product by filtering a search. Or if your organization handles analytical queries in addition to text searching then Elasticsearch is a good choice.


## Reference

* https://www.mongodb.com/resources/basics/full-text-search
* https://sematext.com/blog/solr-vs-elasticsearch-differences
* https://www.knowi.com/blog/what-is-elastic-search/
* https://en.wikipedia.org/wiki/Apache_Solr
