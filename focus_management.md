# Focus Management


### What is Deep Work?

It is focusing without distraction on a cognitively demanding task.


### How to do deep work properly?

* Intense periods of focus in isolated fields of work cause myelin to develop in relevant areas of the brain. 
* Myelin allows brain cells to fire faster and cleaner which allows our brain to rapidly connect ideas and uncover creative solutions.


### How can you implement the principles in your day-to-day life?

* Working in the early morning so that there will be no distractions.
* Keeping mobile out of our reach.
* Got into some isolated place if possible.
* Keeping a separate time for social media time.
* Doing deep work at the same time every day.


### What are the dangers of social media?

* It is built to make the user as addictive as possible as it maximizes the profit.
* It replicates an activity that does not produce a lot of value.
* It causes us to lose our concentration.
* The more we use social media the more likely we are to feel lonely.
