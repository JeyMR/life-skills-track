# Good Practices for Software Development


### Which points were new to you?

* Getting frequent feedback on the implementation phase.
* Using group chat/channels to communicate.
* Making some time for team members to improve the communication.


### Which area do you think you need to improve on? What are your ideas to make progress in that area?

I required some improvement in knowing our teammates, as I am not a talkative person. But I am working on this part, and I have some ideas which follow:

* Having breakfast/lunch together will create an opportunity to talk.
* Do not hesitate to ask questions.
* Listening carefully when they are talking, so that it leads to a good conversation.
