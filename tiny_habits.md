# Tiny Habits



## Tiny Habits - BJ Fogg


### What was the most interesting story or idea for you?

The idea of creating a new habit by shrinking it as tiny as possible, then doing it regularly and after some point it will have grown significantly.



## Tiny Habits by BJ Fogg - Core Message


### How can you use B = MAP to make making new habits easier? What are M, A, and P?

* M - Motivation
* A - Ability
* P - Prompt

by applying B = MAP, we are shrinking a habit as small as possible which takes little motivation to do. Therefore, it is highly likely that we follow the tiny habit every day. As we do that every day, the habit will become stronger.


### Why it is important to "Shine" or Celebrate after each successful completion of a habit?

It will increase our confidence and motivation and that in turn makes us repeat that habit and other related habits.



## 1% Better Every Day Video


### In this video, what was the most interesting story or idea for you?

I like the story of photography where they split photographers into two groups and grade one group in terms of quantity and another group in terms of quality. Surprisingly all the best frames came from the quantity group whereas the quality group just clicked the mediocre pictures. This implies that repetition is the key to the mastery.



## Book Summary of Atomic Habits


### What is the book's perspective on identity?

Identity is believing that we are a particular person, which triggers action to become that particular person as we require evidence to believe, which in turn is likely to produce the desired outcome.


### Write about the book's perspective on how to make a habit easier to do?

Habits can be made easier by reducing the friction. For instance, focusing on the first tiny step of the habits which in turn triggers the following steps, and architecting our environment in such a way to trigger those habits.


### Write about the book's perspective on how to make a habit harder to do?

Habits can be made harder by adding more steps to do that so that our frequency of doing that habit is gradually reduced to none.



## Reflection


### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I would like to learn more. Every day after dinner I will learn or revise at least one concept, when I successfully do this I will celebrate. And I will track the progress by marking off all the successful days.


### Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

I would like to reduce my weight. So whenever I see stairs I take them instead of a lift, and I uninstall Swiggy, Zomato, and related apps to reduce the frequency of eating outside food.



