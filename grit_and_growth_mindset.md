# Grit and Growth Mindset


## Grit.

Grit is perseverance in face of the failures. It is a core skill that makes one excel in any field. And it is not related to talent.


## Introduction to Growth Mindset.

There are two key characteristics of a growth mindset

* Believes that skills can be built.
* Focuses on the process instead of the result.

with these mindsets, one puts effort into learning things that are difficult for him/her, shows willingness to face challenges, learn from failures, and appreciate feedback.


## Understanding Internal Locus of Control.

Internal Locus of Control implies that we have control over the outcomes. A person who is having this will try to put effort into getting the desired results. The point is we should believe the results we get via our hard work and efforts, in other words, we are responsible for the outcomes.


## How to build a Growth Mindset.

* Believes one's abilities.
* Questions one's assumption.
* Designs one's curriculum for life, which could be a set of books.
* Honors one's struggle.


## What are your ideas to take action and build a Growth Mindset.

* Taking responsibility for all my actions.
* Keep trying until I reach the desired outcomes.
* Taking breaks instead of quitting when I got exhausted.
* Celebrating small successes to keep me motivated.
* believe that struggle comes only to make me stronger.
* keep remembering that anything can be learned no matter how difficult it is.
