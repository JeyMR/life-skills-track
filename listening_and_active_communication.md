# Listening and Active Communication



## Active Listening.


### What are the steps/strategies to do Active Listening?

* Avoid getting distracted from our thoughts.
* Focus on the topic discussed by the speaker.
* Do not interrupt the speaker.
* Show interest to keep the person talking.
* Listen with body language.
* Take notes if appropriate.
* Paraphrase what others have said to make sure you understand correctly.



## Reflective Listening.


### What are the key points of Reflective Listening?

* Sincere listening to the speaker.
* Understanding the hidden feeling.
* Clarifying with empathy what the speaker has said.
* Making the speaker comfortable to open up their feeling.



## Reflection.


### What are the obstacles in your listening process?

I often lose my focus whenever I find the topic boring. When it happens my mind starts to wander immediately, at the time I bring back my focus at present I might have missed the points from the speaker.


### What can you do to improve your listening?

I can apply the strategies from Active Listening. If the speaker is emotional, I can use Reflective Listening.



## Types of Communication.


### When do you switch to a Passive communication style in your day-to-day life?

I switch to Passive communication when I realize the other person is using Passive communication for too long.


### When do you switch to Aggressive communication styles in your day-to-day life?

When something crossed the limits, I could not take it anymore. I will switch to Aggressive communication.


### When do you switch to passive-aggressive communication styles in your day-to-day life?

In a situation, where something is annoying. I will switch to passive-aggressive.


### How can you make your communication assertive?

* Recognizing my needs.
* Being open to others about my needs.
* Respecting other's needs as well, trying to avoid conflict.
* Being honest. 
* Avoid hesitation.


