# Learning Process



## How to learn faster with the Feynman Technique.


### What is the Feynman Technique?

It is a technique to test your understanding by explaining the concepts to someone, typically to kids, who do not have a basic knowledge of that.



## Learning how to learn TED talk by Barbara Oakley.


### In this video, what was the most interesting story or idea for you?

There are several ideas I found very interesting
* Active modes and diffused modes.
* Avoid procrastination using the Pomodoro technique.
* How to achieve true mastery.


### What are active and diffused modes of thinking?

In active mode, our brain tries to fit a new concept into an already-defined pattern where the concept might require a new pattern. This mode applies when we are focused. In passive mode, our brain allows us to form a new pattern which in turn allows us to understand the concept deeply. This mode applies when we are loosely focused.



## Learn anything in 20 hours.


### What are the steps to take when approaching a new topic?

* Deconstruct the skill.
* Learn enough to self-correct.
* Remove practice barriers.
* Practice at least 20 hours.



## Learning Principles.


### What are some of the actions you can take going forward to improve your learning process?

The following ideas I will apply in my learning.
* Feynman Technique.
* Combine Third-Time with Pomodoro Technique:
  * Minimum time: 25 minutes.
  * Maximum time: as much as I like.
  * Break duration: as per the Third-Time rule.
* Swap between active and diffused modes whenever required.
* Deep work: get rid of all distractions while studying.


