# Energy Management



## Manage Energy not Time.


### What are the activities you do that make you relax - Calm quadrant?

* Meditation.
* Talk with brothers/sisters.
* Play with nephews/nieces.
* Listen to music while walking.
* Watch some comedy shows.
* Ride with friends.
* Visit a bookstore.


### When do you find getting into the Stress quadrant?

I found myself in the stress quadrant while facing something which I was not prepared for.


### How do you understand if you are in the Excitement quadrant?

When I am in the excitement, I feel so happy, temporarily forget my problems, and look forward to the event which makes me very excited.



## Sleep is your Superpower.


### Points from Sleep is your Superpower.

* Lack of sleep will age a person a decade.
* Sleep enhances the Learning.
* Sleep loss increases heart attacks.
* Sleep increases our Natural Killer Cells.
* Short sleep causes the risk of developing cancer or Alzheimer's disease.
* Lack of sleep causes the modification of genes.
* Sleep is a non-negotiable biological necessity.


### What are some ideas that you can implement to sleep better?

* Avoid having caffeine at night.
* Regularity: maintaining the same time for sleeping every day.
* Keeping our room temperature cold.



## Brain Changing Benefits of Exercise.


### Points from Brain Changing Benefits of Exercise.

* Prefrontal Cortex is critical for things like decision-making, focus, attention, and personality.
* The hippocampus is critical for the ability to form and retain new long-term memories of facts and events.
* Exercises have immediate effects on our brain.
* A single workout can improve our ability to shift and focus attention, and that focus improvement will last for at least two hours.
* Exercises change the brain's anatomy, physiology, and function.
* Exercises produce brand new brain cells. New cells in the hippocampus increase its volume, as well as improve our long-term memory.
* Long-term exercises improve attention function dependent on our prefrontal cortex.
* The more we work the bigger and stronger our hippocampus and prefrontal cortex gets.


### What are some steps you can take to exercise more?

* Taking stairs instead of lifts.
* Prefer walking if the destination is within a 1 km range.
* Playing badminton or any other sports at least on weekends.

