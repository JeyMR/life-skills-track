# Prevention of Sexual Harassment


### What kinds of behavior cause sexual harassment?

* Verbal Harassment
  * Comments about clothing, a person's body, and gender-based jokes.
  * Repeatedly asking a person out.
  * Sexual innuendos.
  * Spreading rumors about a person's personal or sexual life.
  * Using foul and obscene language. 
  
* Visual Harassment
  * Obscene posters, drawings, pictures, screen savers, cartoons, emails, or texts of a sexual nature.
  
* Physical Harassment
  * Sexual assault.
  * Impeding or blocking movement.
  * Inappropriate touching such as kissing, hugging, patting, stroking, and rubbing.
  * Sexual gesturing such as leering or staring.
  
  
### What would you do in case you face or witness any incident or repeated incidents of such behavior?

First I would inform the person who is bothering me. If the person is not listening, I would report him/her to the supervisor as per the company rules.

